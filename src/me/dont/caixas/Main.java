package me.dont.caixas;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import me.dont.caixas.Caixa.Tipo;


public class Main extends JavaPlugin{
	
	public static HashMap<String, Caixa> caixas;
	public static HashMap<String, Raridade> raridades;
	
	public static Economy economia;
	public static String npc;
	public static String profissao;
	public static boolean buy;
	public static int tamanho;
	public static Sound som;
	public static String nome;


	public void onEnable(){
		org.bukkit.Bukkit.getConsoleSender().sendMessage("�5doCaixas ativado!");
		Register();
		saveDefaultConfig();
		Caixas();
		profissao = this.getConfig().getString("NPC.Profissao");
		nome = this.getConfig().getString("Menu.Nome");
		npc = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("NPC.Nome"));
		economia = Economy.valueOf(this.getConfig().getString("NPC.Economia").toUpperCase());
		buy = this.getConfig().getBoolean("Menu.AtivarCompra");
		tamanho = this.getConfig().getInt("Menu.Tamanho");
		som = Sound.valueOf(getConfig().getString("Geral.Som").toUpperCase());
		System.out.println(economia);
		new BukkitRunnable() {
			@Override
			public void run() {
				Economia.setEconomy();
				
			}
		}.runTaskLaterAsynchronously(Main.getPlugin(Main.class), 20L*10);
	}

	   static <K,V extends Comparable<? super V>>
	    SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
	        SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
	            new Comparator<Map.Entry<K,V>>() {
	                @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
	                    int res = e2.getValue().compareTo(e1.getValue());
	                    return res != 0 ? res : 1;
	                }
	            }
	        );
	        sortedEntries.addAll(map.entrySet());
	        return sortedEntries;
	    }
	
	@SuppressWarnings("deprecation")
	public void Caixas(){
		caixas = new HashMap<>();
		raridades = new HashMap<>();
		FileConfiguration cfg = this.getConfig();
		String s = "Caixas.";
		for(String key : this.getConfig().getConfigurationSection("Caixas").getKeys(false)){
			HashMap<HashMap<Tipo, ItemStack>, Integer> mapa = new HashMap<>();
			for(String items : this.getConfig().getConfigurationSection("Caixas."+key+".Itens").getKeys(false)){
				HashMap<Tipo, ItemStack> toPut = new HashMap<>();
				String c = "Caixas."+key+".Itens."+items+".";
				String id = cfg.getString(c+"ID");
				String quantia = cfg.getString(c+"Quantia");
				String data = cfg.getString(c+"Data");
				String enchants = cfg.getString(c+"Encantamentos");
				String name = cfg.getString(c+"Nome");
				String lores = cfg.getString(c+"Lore");
				String cmd = cfg.getString(c+"Comando");
				String glow = cfg.getString(c+"Glow");
				String rar = cfg.getString(c+"Raridade");
				ItemStack stack = new ItemBuilder(id,quantia).setDurability(data).setGlow(glow).addEnchantments(enchants).setName(name).setLore(lores,cmd ,rar).toItemStack();
				toPut.put(Tipo.ITEM, stack);
				mapa.put(toPut, Integer.valueOf(cfg.getString(c+"Chance")));
			}

			
			
			java.util.List<String> lores = new ArrayList<>();
			int chance = 0;
			for (int a : mapa.values()){
				chance += a;
			}
			for (String bc : this.getConfig().getStringList(s+key+".Lore")){
				lores.add(ChatColor.translateAlternateColorCodes('&', bc));
			}
			java.util.List<String> lorespreco = new ArrayList<>();

			for (String bc : this.getConfig().getStringList(s+key+".LorePreco")){
				lorespreco.add(ChatColor.translateAlternateColorCodes('&', bc));
			}
			Caixa c = new Caixa(key,this.getConfig().getString(s+key+".Nome"), Material.getMaterial(this.getConfig().getInt(s+key+".ID")), mapa,lores, this.getConfig().getBoolean(s+key+".Glow"), this.getConfig().getInt(s+key+".Preco"), this.getConfig().getInt(s+key+".Slot"), lorespreco);
			caixas.put(key, c);
			if (chance > 100){
				Bukkit.getLogger().log(Level.WARNING, "[doCaixas] As chances da caixa "+c.getIdentifier()+" nao estao baseadas em porcentagem");
				
			}
		}
		for(String key : this.getConfig().getConfigurationSection("Raridades").getKeys(false)){
			String id = "Raridades."+key+".";
			Raridade r = new Raridade(key, this.getConfig().getBoolean(id+"Broadcast"), this.getConfig().getBoolean(id+"Raio"), this.getConfig().getString(id+"BroadcastMsg"));
			raridades.put(key, r);
		}
		
		

		
	}
	
	
	public void Register(){
		getCommand("darcaixa").setExecutor(new DarCaixa());
		getCommand("darcaixa").setTabCompleter(new DarCaixa());
		getCommand("setcaixanpc").setExecutor(new NPC());
		getCommand("caixas").setExecutor(new Comando());

		getServer().getPluginManager().registerEvents(new Cancel(), this);
		getServer().getPluginManager().registerEvents(new AbrirCaixa(), this);
		getServer().getPluginManager().registerEvents(new NPC(), this);
	}


	
}
