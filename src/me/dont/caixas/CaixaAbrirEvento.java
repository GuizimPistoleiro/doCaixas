package me.dont.caixas;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public final class CaixaAbrirEvento extends Event implements Cancellable{
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private Caixa caixa;
    private Player player;
    
    public CaixaAbrirEvento(Caixa c, Player p) {
    	caixa = c;
    	player = p;
    }

    public Caixa getCaixa() {
        return caixa;
    }
    
    public Player getPlayer(){
    	return player;
    }
    

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean arg0) {
		cancelled = arg0;
	}
}
	
