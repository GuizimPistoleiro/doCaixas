package me.dont.caixas;

import net.md_5.bungee.api.ChatColor;

public class Raridade {

	private String identifier;
	private String broadcastmensagem;
	private boolean broadcast;
	private boolean raio;

	
	public Raridade(String identificador, boolean broadcast, boolean raio, String broadcastmsg){
		identifier = identificador;
		this.broadcast = broadcast;
		this.raio = raio;
		broadcastmensagem = ChatColor.translateAlternateColorCodes('&', broadcastmsg);
	}
	
	
	public String getIdentifier(){
		return identifier;
	}
	
	public void setIdentifier(String s){
		identifier = s;
	}
	
	public String getBroadcastMsg(){
		return broadcastmensagem;
	}
	
	public void setBroadcastMsg(String s){
		broadcastmensagem = s;
	}
	
	public boolean isBroadcast(){
		return broadcast;
	}
	
	public boolean isRaio(){
		return raio;
	}
	
}
